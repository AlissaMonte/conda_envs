# Conda Environments
README last updated: **2021/11/22**

Environment last updated: **2021/11/21**

## Installation and setup of Conda environment
First check to see if Conda is installed.
```
which conda
```
If Conda is not installed, here are some basic installation instructions (Skip to env creation otherwise):
```
#Where are you?
export CURRENTDIR=`pwd`
# Go to HOME directory
cd
# Run install script
bash $CURRENTDIR/Miniconda3-latest-Linux-x86_64.sh
# Source conda to get it running
source ${HOME}/miniconda3/etc/profile.d/conda.sh
```
Then in your `.bashrc` file, copy and paste the following line at the bottom of the file:
```
source ${HOME}/miniconda3/etc/profile.d/conda.sh
```
Now let's create the environment using the yaml file in the repo.
```
conda env create -f $CURRENTDIR/environment.yml
```
This will create an environment called `conda_env_21_11_2021`, activate the environment using the following command:
```
conda activate conda_env_21_11_2021
```
Copy this command at the bottom of your `.bashrc` file. Now everytime you open a terminal, the Conda environment will be activated.

## Useful conda commands

- The pip module is installed in the environment so to install additional python modules use the commmand below. Replace the `venv_name` with the environment name you're using and `package_name` with the python package you would like to install.

```
/miniconda3/envs/venv_name/bin/pip install package_name
```
- Deactivating Conda
```
conda deactivate
```
- Search Conda for package.
```
conda search package_name
```
- List Conda environments
```
conda env list
```
***
See [link](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf) for conda cheatsheet.
